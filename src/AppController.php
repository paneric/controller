<?php

declare(strict_types=1);

namespace Paneric\Controller;

use Paneric\Interfaces\Session\SessionInterface;
use Twig\Environment as Twig;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

abstract class AppController
{
    protected $twig;
    protected $session;

    public function __construct(Twig $twig, SessionInterface $session)
    {
        $this->twig = $twig;
        $this->session = $session;
    }

    protected function render(Response $response, string $template, array $data = [], int $status = null): Response
    {
        try {
            $response->getBody()->write(
                $this->twig->render($template, $data)
            );

            $response = $response->withHeader('Content-Type', 'text/html;charset=utf-8');

            if (!is_null($status)) {
                return $response->withStatus($status);
            }

            return $response;
        } catch (LoaderError $e) {
            echo $e->getMessage();
        } catch (RuntimeError $e) {
            echo $e->getMessage();
        } catch (SyntaxError $e) {
            echo $e->getMessage();
        }

        return null;
    }

    protected function redirect(Response $response, string $url, int $status = null): Response
    {
        $response = $response->withHeader('Location', (string)$url);

        if (!is_null($status)) {
            return $response->withStatus($status);
        }

        return $response;
    }

    protected function addFlash(string $message, string $key): void
    {
        $this->session->setFlash([$message], $key);
    }
}