<?php

declare(strict_types=1);

namespace Paneric\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use RuntimeException;

abstract class ApiController
{
    protected $service;


    protected function jsonResponse(
        Response $response,
        array $data = null,
        int $status = null,
        int $encodingOptions = 0)
    : Response {
        $response->getBody()->write(
            $json = json_encode($data, $encodingOptions)
        );

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

        if (isset($status)) {
            return $response->withStatus($status);
        }

        return $response;
    }
}
